unit UnitHome;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  THome = class(TForm)
    EditPokemonName: TEdit;
    LabelPokemon: TLabel;
    ButtonAdd: TButton;
    ButtonDelete: TButton;
    ButtonDeleteAll: TButton;
    ListBoxPokemon: TListBox;
    procedure ButtonAddClick(Sender: TObject);
    procedure ButtonDeleteClick(Sender: TObject);
    procedure ButtonDeleteAllClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Home: THome;

implementation

{$R *.dfm}

procedure THome.ButtonAddClick(Sender: TObject);
begin
  ListBoxPokemon.Items.Add(EditPokemonName.Text);
end;

procedure THome.ButtonDeleteAllClick(Sender: TObject);
begin
  ListBoxPokemon.Clear;
end;

procedure THome.ButtonDeleteClick(Sender: TObject);
begin
  ListBoxPokemon.DeleteSelected;
end;

end.
