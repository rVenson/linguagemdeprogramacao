# Linguagem de Programação
Disciplina de Linguagem de Programação
Terceiro Ano do Ensino Médio Integrado em Informática
Centro de Educação Profissional Abílio Paulo

**Carga Horária**: 5 aulas semanais
**Semanas**: 44 semanas*
> \* Valor aproximado

## Ementa (Oficial)

 - Introdução ao ambiente de desenvolvimento
 - Conhecimento do ambiente de programação
 - Criação de procedimentos e funções (modelos de passagens de parâmetros)
 - Constantes
 - Operadores
 - Variáveis e tipos de dados
 - Escopo
 - Estruturas condicionais
 - Estruturas de repetição
 - Estruturas de controle
 - Criação de procedimentos e funções
 - Tratamento de exceções
 - Criação de menus
 - Conceitos básicos de orientação à objeto (polimorfismo, encapsulamento e herança)
 - Programação orientada à objeto
 - Objetos e classes
 - Atributos e comportamento
 - Método de programação avançada
 - Conexão com banco de dados
 - Estudo de Caso: Projeto de Conclusão de Curso

## Plano de Ensino

### Primeiro Trimestre

 - [x] Revisão de Estruturas de Decisão
 - [x] Revisão de Estruturas de Repetição
 - [x] Introdução ao Git
 - [x] Introdução ao Gitlab
 - [x] Introdução à metodologias ágeis
 - [x] Conceitos de Orientação à Objeto
	 - [x] Polimorfismo
	 - [x] Encapsulamento
	 - [x] Herança

### Segundo Trimestre

 - [ ] Componentes básicos do Embarcadero Delphi
 - [ ] Conexão com Banco de Dados (MySQL)
 - [ ] Relatórios

### Terceiro Trimestre

 - [ ] Trabalho de Conclusão de Curso

## Calendário

| Bimestre | Início | Fim |
|--|--|--|
| Primeiro | 11 de fevereiro | 17 de maio |
| Segundo | 20 de maio | 07 de setembro |
| Terceiro | 09 de setembro | 16 de dezembro |
